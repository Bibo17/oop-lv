﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		List<string> wordi = new List<string>();
		Game i;
		private void Form1_Load(object sender, EventArgs e)
		{
			using (System.IO.StreamReader reader = new System.IO.StreamReader("ListaRijeci.txt"))
			{
				string line;
				line = reader.ReadLine();
				string[] parts = line.Split(',');
				foreach (string s in parts)
					wordi.Add(s);
			}
			Random rnd = new Random();
			int n = rnd.Next(0, wordi.Count - 1);
			string clue = wordi[n];
			i = new Game(clue);
			i.Ispis();
			labelWord.Text = i.forLabel;
			labelLifeCount.Text = i.LifeNo.ToString();
		}
		public class Game
		{
			public int LifeNo { get; private set; }
			public string word { get; private set; }
			public List<char> Attempts { get; set; }
			private string GuessedLetters = String.Empty;
			public int GuessedLettersCount { get; private set; }
			public string forLabel { get; private set; }
			public Game(string r)
			{
				word = r;
				LifeNo = 6;
				Attempts = new List<char>();
				GuessedLettersCount = 0;
			}
			public bool IsFound(char c)
			{
				foreach (char d in GuessedLetters)
				{
					if (d == c) return true;
				}
				return false;
			}
			public void Ispis()
			{
				forLabel = String.Empty;
				foreach (char c in word)
				{
					if (c == ' ') forLabel += c;
					else if (IsFound(c)) forLabel += c;
					else forLabel += '_';
					forLabel += ' ';
				}
			}
			public void Guess(char c)
			{
				c = char.ToUpper(c);
				Attempts.Add(c);
				foreach (char d in word)
				{
					if (d == c)
					{
						GuessedLettersCount++;
						GuessedLetters += c;
					}
				}
				if (!IsFound(c)) LifeNo--;
			}
			public int spaceCount()
			{
				int space = 0;
				foreach (char c in word)
					if (c == ' ') space++;
				return space;
			}
			public bool AlreadyTried(char c)
			{
				foreach (char d in Attempts)
					if (c == d) return true;
				return false;
			}
		}

		private void buttonPogadjaj_Click(object sender, EventArgs e)
		{
			if (textBoxSlovo.Text == String.Empty) MessageBox.Show("Niste unijeli slovo!", "Pogreška");
			else if (i.AlreadyTried(char.ToUpper(textBoxSlovo.Text[0]))) { MessageBox.Show("Već ste pogađali to slovo!", "Pogreška"); textBoxSlovo.Text = String.Empty; }
			else
			{
				i.Guess(textBoxSlovo.Text[0]);
				i.Ispis();
				labelWord.Text = i.forLabel;
				labelLifeCount.Text = i.LifeNo.ToString();
				textBoxSlovo.Text = String.Empty;
				if (i.LifeNo == 0)
				{
					MessageBox.Show("Izgubili ste!", "Game over");
					labelWord.Text = i.word;
					labelUnos.Visible = false;
					textBoxSlovo.Visible = false;
					buttonPogadjaj.Visible = false;
					Application.Exit();
				}
				else if (i.GuessedLettersCount + i.spaceCount() == i.word.Length)
				{
					MessageBox.Show("Pobijedili ste!", "Pobjeda");
					buttonPogadjaj.Visible = false;
					labelUnos.Visible = false;
					textBoxSlovo.Visible = false;
					Application.Exit();
				}
			}
		}
	}
}
