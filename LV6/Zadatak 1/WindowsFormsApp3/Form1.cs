﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
	public partial class ZnanstveniKalkulator : Form
	{
		public ZnanstveniKalkulator()
		{
			InitializeComponent();
		}

		private void ZnanstveniKalkulator_Load(object sender, EventArgs e)
		{
			labelRez.Visible = false;
			label5.Visible = false;
		}

		private void buttonIzracunaj_Click(object sender, EventArgs e)
		{
			double op1, op2;
			if (comboBoxOperacije.SelectedItem == null) MessageBox.Show("Niste odabrali operaciju!", "Pogreška");
			else if (textBox1.Text == String.Empty) MessageBox.Show("Niste unijeli prvi operand!", "Pogreška");
			else if (textBox2.Text == String.Empty && textBox2.Visible == true) MessageBox.Show("Niste unijeli drugi operand!", "Pogreška");
			else if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Prvi operand nije broj!", "Pogreška");
			else if (!double.TryParse(textBox2.Text, out op2) && textBox2.Visible == true) MessageBox.Show("Drugi operand nije broj!", "Pogreška");
			else
			{
				if (comboBoxOperacije.SelectedItem.ToString() == "+")
				{
					labelRez.Text = textBox1.Text + " + " + textBox2.Text + " = " + (op1 + op2).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "-")
				{
					labelRez.Text = textBox1.Text + " - " + textBox2.Text + " = " + (op1 - op2).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "*")
				{
					labelRez.Text = textBox1.Text + " * " + textBox2.Text + " = " + (op1 * op2).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "/")
				{
					if (op2 == 0) MessageBox.Show("Dijeljenje s nulom nije moguće!", "Pogreška");
					else
					{
						labelRez.Text = textBox1.Text + " / " + textBox2.Text + " = " + (op1 / op2).ToString();
						textBox1.Text = string.Empty;
						textBox2.Text = string.Empty;
						labelRez.Visible = true;
						label5.Visible = true;
					}
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "√")
				{
					if (op1 < 0) MessageBox.Show("Nije moguće računati drugi korijen negativnog broja!", "Pogreška");
					else
					{
						labelRez.Text = "√" + textBox1.Text + " = " + Math.Sqrt(op1).ToString();
						textBox1.Text = string.Empty;
						textBox2.Text = string.Empty;
						labelRez.Visible = true;
						label5.Visible = true;
						textBox1.Text = string.Empty;
						textBox2.Text = string.Empty;
						labelRez.Visible = true;
						label5.Visible = true;
					}
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "x^2")
				{
					labelRez.Text = textBox1.Text + "^2 = " + (op1 * op1).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "sin")
				{
					labelRez.Text = "sin(" + textBox1.Text + ") = " + Math.Sin(op1).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "cos")
				{
					labelRez.Text = "cos(" + textBox1.Text + ") = " + Math.Cos(op1).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
				else if (comboBoxOperacije.SelectedItem.ToString() == "log")
				{
					labelRez.Text = "log(" + textBox1.Text + ") = " + Math.Log10(op1).ToString();
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
					textBox1.Text = string.Empty;
					textBox2.Text = string.Empty;
					labelRez.Visible = true;
					label5.Visible = true;
				}
			}
		}

		private void comboBoxOperacije_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (comboBoxOperacije.SelectedItem.ToString())
			{
				case "+":
				case "-":
				case "*":
				case "/": textBox2.Visible = true; label2.Visible = true; break;
				default: textBox2.Visible = false; label2.Visible = false; break;
			}
		}

		private void label2_Click(object sender, EventArgs e)
		{

		}
	}
	
}
