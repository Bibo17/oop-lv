﻿namespace WindowsFormsApp3
{
	partial class ZnanstveniKalkulator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.comboBoxOperacije = new System.Windows.Forms.ComboBox();
			this.buttonIzracunaj = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.labelRez = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(21, 67);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(93, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Prvi operand:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(261, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(103, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Drugi operand:";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(131, 67);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 22);
			this.textBox1.TabIndex = 2;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(370, 69);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(100, 22);
			this.textBox2.TabIndex = 3;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(41, 120);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(73, 17);
			this.label4.TabIndex = 5;
			this.label4.Text = "Operacija:";
			// 
			// comboBoxOperacije
			// 
			this.comboBoxOperacije.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxOperacije.FormattingEnabled = true;
			this.comboBoxOperacije.Items.AddRange(new object[] {
            "+",
            "-",
            "*",
            "/",
            "√",
            "x^2",
            "sin",
            "cos",
            "log"});
			this.comboBoxOperacije.Location = new System.Drawing.Point(131, 120);
			this.comboBoxOperacije.Name = "comboBoxOperacije";
			this.comboBoxOperacije.Size = new System.Drawing.Size(121, 24);
			this.comboBoxOperacije.TabIndex = 6;
			this.comboBoxOperacije.SelectedIndexChanged += new System.EventHandler(this.comboBoxOperacije_SelectedIndexChanged);
			// 
			// buttonIzracunaj
			// 
			this.buttonIzracunaj.Location = new System.Drawing.Point(288, 120);
			this.buttonIzracunaj.Name = "buttonIzracunaj";
			this.buttonIzracunaj.Size = new System.Drawing.Size(91, 32);
			this.buttonIzracunaj.TabIndex = 7;
			this.buttonIzracunaj.Text = "Izracunaj";
			this.buttonIzracunaj.UseVisualStyleBackColor = true;
			this.buttonIzracunaj.Click += new System.EventHandler(this.buttonIzracunaj_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(60, 191);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 17);
			this.label5.TabIndex = 8;
			this.label5.Text = "Rezultat:";
			// 
			// labelRez
			// 
			this.labelRez.AutoSize = true;
			this.labelRez.Location = new System.Drawing.Point(130, 191);
			this.labelRez.Name = "labelRez";
			this.labelRez.Size = new System.Drawing.Size(46, 17);
			this.labelRez.TabIndex = 9;
			this.labelRez.Text = "label6";
			// 
			// ZnanstveniKalkulator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(509, 248);
			this.Controls.Add(this.labelRez);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.buttonIzracunaj);
			this.Controls.Add(this.comboBoxOperacije);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "ZnanstveniKalkulator";
			this.Text = "Znanstveni Kalkulator";
			this.Load += new System.EventHandler(this.ZnanstveniKalkulator_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboBoxOperacije;
		private System.Windows.Forms.Button buttonIzracunaj;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label labelRez;
	}
}

